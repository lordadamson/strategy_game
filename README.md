# strategy_game
Very basic strategy game using Godot engine

## Screenshots
![Alt text](/screenshots/Screenshot1.jpg?raw=true "Screenshot 1")
![Alt text](/screenshots/Screenshot2.jpg?raw=true "Screenshot 2")

## How to test it?
- Download The latest release candidate of the Godot Engine from over [here](http://www.godotengine.org/projects/godot-engine/documents). <br/>
- Clone this project: `git clone https://github.com/lordadamson/strategy_game.git` <br/>
- Run the engine and import the project.
- Run it.
