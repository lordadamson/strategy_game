extends Node

var state

var senpai
var money

const troop = preload("res://troop.scn")
const IDLE = 0
const AGGRESSIVE = 1

func _ready():
	set_process(true)
	senpai = get_node("/root/senpai")
	senpai.connect("money_changed", self, "_on_money_changed")
	senpai.connect("new_unit", self, "_on_new_unit")
	money = senpai.get_enemy_money()
	state = IDLE

func _process(delta):
	if state == AGGRESSIVE:
		if money >= 30:
			var new_troop = troop.instance()
			new_troop.set_pos(Vector2(500, 600))
			senpai.spend_money(30, 2)
			new_troop.set_team(2)
			senpai.register_unit(new_troop)
			add_child(new_troop)

func _on_money_changed(nonofmybusiness, enemy_money):
	money = enemy_money

func _on_new_unit(unit):
	if unit.get_team() == 1:
		state = AGGRESSIVE