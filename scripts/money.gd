extends Label

func _on_money_changed(money, enemy_money):
	set_text("Money: " + str(money) + "\nEnemy Money: " + str(enemy_money))

func _ready():
	var senpai = get_node("/root/senpai")
	set_text("Money: " + str(senpai.get_money()) + "\nEnemy Money: " + str(senpai.get_enemy_money()))
	senpai.connect("money_changed", self, "_on_money_changed")