extends Node

var senpai
var label

func _ready():
	set_process_input(true)
	
	senpai = get_node("/root/senpai")
	label = get_node("Label")
	var winner = senpai.get_winner()
	label.set_text(winner + " wins!")

func _input(event):
	if event.is_action("click") and not event.is_echo():
		if not event.is_pressed():
			senpai.reset_game()