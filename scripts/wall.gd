extends StaticBody2D

var health = 100
var team

var senpai

signal dead

func _ready():
	senpai = get_node("/root/senpai")
	get_node("CollisionShape2D").get_shape()

func set_team(t):
	team = t

func get_team():
	return team

func decrease_health(amount):
	health -= amount
	if health <= 0:
		die()

func die():
	senpai.increase_money(self)
	emit_signal("dead")
	queue_free()