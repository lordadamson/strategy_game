extends Node

var money
var enemy_money
var units = []
var enemy_units = []
var winner

const START_MONEY = 500
const INCREASE_AMOUNT = 32

signal money_changed
signal new_unit

func _ready():
	money = START_MONEY
	enemy_money = START_MONEY

func get_money():
	return money

func get_enemy_money():
	return enemy_money

func spend_money(amount, team):
	if team == 1:
		money -= amount
	elif team == 2:
		enemy_money -= amount
	emit_signal("money_changed", money, enemy_money)

func increase_money(unit):
	if unit.get_team() == 1:
		enemy_money += INCREASE_AMOUNT
	elif unit.get_team() == 2:
		money += INCREASE_AMOUNT
	emit_signal("money_changed", money, enemy_money)

func register_unit(unit):
	if unit.get_team() == 1:
		units.append(unit)
	elif unit.get_team() == 2:
		enemy_units.append(unit)
	emit_signal("new_unit", unit)

func delete_unit(unit):
	if unit.get_team() == 1:
		units.erase(unit)
	elif unit.get_team() == 2:
		enemy_units.erase(unit)

func get_shortest_distance_between(array, unit):
	if array.size() == 0:
		return null
	var temp_distance
	var nearest_distance = unit.get_pos().distance_to(array[0].get_pos())
	var nearest_enemy = array[0]
	for i in array:
		temp_distance = unit.get_pos().distance_to(i.get_pos())
		if temp_distance < nearest_distance:
			nearest_distance = temp_distance
			nearest_enemy = i
	return nearest_enemy

func get_nearest_enemy(unit):
	if unit.get_team() == 1:
		return get_shortest_distance_between(enemy_units, unit)
	elif unit.get_team() == 2:
		return get_shortest_distance_between(units, unit)

func get_enemy_count(unit):
	if unit.get_team() == 1:
		return enemy_units.size()
	elif unit.get_team() == 2:
		return units.size()

func reset_game():
	money = START_MONEY
	enemy_money = START_MONEY
	units = []
	enemy_units = []
	set_scene("res://main.scn")

func set_scene(scene):
	var current_scene = get_tree().get_root().get_child( get_node("/root").get_child_count() - 1)
	current_scene.queue_free()
	current_scene = load(scene)
	get_tree().get_root().add_child( current_scene.instance() )

func declare_loss(unit):
	if unit.get_team() == 1:
		winner = "Enemy"
	elif unit.get_team() == 2:
		winner = "Adam"
	set_scene("res://game_over.scn")

func get_winner():
	return winner