extends Node

var mouse_position
var object_at_cursor #when player clicks a unit button, a copy of that unit follows the cursor to indicate where it should be placed. a reference to this unit is placed here.
var soldier_btn
var wall_H_btn
var wall_V_btn

var state

const troop = preload("res://troop.scn")
const wall_H = preload("res://wall_H.scn")
const wall_V = preload("res://wall_V.scn")
const NORMAL = 0
const OBJECT_BTN_CLICKED = 1
const OBJECT_BTN_CLICKED_HIDDEN = 2
var hidden_object

func _ready():
	set_process(true)
	set_process_input(true)
	senpai = get_node("/root/senpai")
	soldier_btn = get_node("Panel/soldier_btn")
	wall_H_btn = get_node("Panel/wall_H_btn")
	wall_V_btn = get_node("Panel/wall_V_btn")
	soldier_btn.connect("pressed", self, "_on_soldier_btn_pressed")
	wall_H_btn.connect("pressed", self, "_on_wall_H_btn_pressed")
	wall_V_btn.connect("pressed", self, "_on_wall_V_btn_pressed")
	state = NORMAL

func attach_unit_to_cursor(unit):
	state = OBJECT_BTN_CLICKED
	object_at_cursor = weakref(unit.instance())
	mouse_position = get_viewport().get_mouse_pos()
	object_at_cursor.get_ref().set_pos(mouse_position)
	add_child(object_at_cursor.get_ref())
	object_at_cursor.get_ref().get_node("CollisionShape2D").queue_free() #so that it wouldn't interact with the world before getting spawned
	if hidden_object == "troop":
		object_at_cursor.get_ref().set_team(1)
	hidden_object = ""

func _on_wall_H_btn_pressed():
	if senpai.get_money() >= 30:
		hidden_object = "wall_H"
		state = OBJECT_BTN_CLICKED_HIDDEN

func _on_wall_V_btn_pressed():
	if senpai.get_money() >= 30:
		hidden_object = "wall_V"
		state = OBJECT_BTN_CLICKED_HIDDEN

func _on_soldier_btn_pressed():
	if senpai.get_money() >= 30:
		state = OBJECT_BTN_CLICKED_HIDDEN
		hidden_object = "troop"

func on_mouse_inside_panel():
	if state == OBJECT_BTN_CLICKED: #if I have an object selected, hide it temporarily until I leave again
		state = OBJECT_BTN_CLICKED_HIDDEN
		hidden_object = object_at_cursor.get_ref().get_name()
		object_at_cursor.get_ref().queue_free()

func on_mouse_outside_panel(): # when I get out, I see if there was something selected previously and I restore it
	if state == OBJECT_BTN_CLICKED_HIDDEN:
		state = NORMAL
		if hidden_object == "troop":
			attach_unit_to_cursor(troop)
		elif hidden_object == "wall_H":
			attach_unit_to_cursor(wall_H)
		elif hidden_object == "wall_V":
			attach_unit_to_cursor(wall_V)
		

func _process(delta):
	mouse_position = get_viewport().get_mouse_pos()
	if state == OBJECT_BTN_CLICKED:
		if object_at_cursor.get_ref():
			object_at_cursor.get_ref().set_pos(mouse_position)
	if mouse_position.y >= 980:
		on_mouse_inside_panel()
	else:
		on_mouse_outside_panel()
	

func _input(event):
	if state == OBJECT_BTN_CLICKED and event.is_action_pressed("click"): #look at the input map at the project settings
		spawn_unit()
	elif state == OBJECT_BTN_CLICKED and event.is_action_released("right_click"):
		reset_state()

func reset_state():
	if object_at_cursor.get_ref():
		object_at_cursor.get_ref().queue_free()
	state = NORMAL

func spawn_unit():
	if object_at_cursor.get_ref().get_name() == "troop":
		if senpai.get_money() >= 30:
			var new_troop = troop.instance()
			mouse_position = get_viewport().get_mouse_pos()
			new_troop.set_pos(mouse_position)
			senpai.spend_money(30, 1)
			new_troop.set_team(1)
			senpai.register_unit(new_troop)
			add_child(new_troop)
	elif object_at_cursor.get_ref().get_name() == "wall_H":
		if senpai.get_money() >= 30:
			var new_wall = wall_H.instance()
			spawn_wall(new_wall)
	elif object_at_cursor.get_ref().get_name() == "wall_V":
		if senpai.get_money() >= 30:
			var new_wall = wall_V.instance()
			spawn_wall(new_wall)

func spawn_wall(new_wall):
	mouse_position = get_viewport().get_mouse_pos()
	new_wall.set_pos(mouse_position)
	senpai.spend_money(30, 1)
	new_wall.set_team(1)
	add_child(new_wall)