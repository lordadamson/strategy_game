extends RigidBody2D

var health = 100
var health_bar
var senpai
var team

signal dead

func _ready():
	senpai = get_node("/root/senpai")
	health_bar = get_node("health_bar")
	
	if get_name() == "home":
		team = 1
	elif get_name() == "enemy":
		team = 2
		
	senpai.register_unit(self)

func decrease_health(amount):
	health -= amount
	health_bar.set_value(health)
	if health <= 0:
		die()

func die():
	senpai.increase_money(self)
	senpai.delete_unit(self)
	emit_signal("dead")
	senpai.declare_loss(self)
	queue_free()

func get_team():
	return team