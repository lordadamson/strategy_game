extends RigidBody2D

var health = 40
var team = 1
var state

var current_target
var current_position
var enemy_position
var direction
var reached_target = false
var timer
var senpai
var health_bar


const IDLE = 0
const AGGRESSIVE = 1

const ATTACK_POWER = 30

signal dead

func _ready():
	self.set_fixed_process(true)
	if team == 1:
		get_node("Sprite").set_texture(load("res://assets/sprites/troop.png"))
	elif team == 2:
		get_node("Sprite").set_texture(load("res://assets/sprites/troop2.png"))
	senpai = get_node("/root/senpai")
	senpai.connect("new_unit", self, "_on_new_unit")
	self.connect("body_enter", self, "_on_body_enter")
	timer = get_node("Timer")
	timer.connect("timeout", self, "_on_timer_finished")
	health_bar = get_node("health")
	health_bar.set_value(health)
	get_new_target()

func _on_new_unit(unit):
	if unit.get_team() != team:
		get_new_target()

func connect_to_target():
	if current_target.get_ref().is_connected("dead", self, "_on_target_dead"):
		current_target.get_ref().connect("dead", self, "_on_target_dead")

func get_new_target():
	var new_target = get_nearest_enemy()
	if new_target != null:
		current_target = weakref(new_target)
		if current_target.get_ref():
			state = AGGRESSIVE
			reached_target = false
			connect_to_target()
	else:
		state = IDLE

func _on_target_dead():
	timer.stop()
	get_new_target()

func _on_timer_finished():
	if current_target.get_ref():
		attack(current_target)
	else:
		_on_target_dead()

func attack(target):
	if target.get_ref():
		target.get_ref().decrease_health(ATTACK_POWER)
		if target.get_ref():
			timer.set_wait_time(2)
			timer.start()

func _on_body_enter(body):
	if state == AGGRESSIVE:
		if current_target.get_ref() and current_target.get_ref() == body:
			reached_target = true
			attack(current_target)
		elif body.get_team() != team and not current_target.get_ref():
			reached_target = true
			current_target = weakref(body)
			connect_to_target()
			attack(current_target)

func is_colliding_with(target):
	for i in get_colliding_bodies():
		if i == target:
			return true
	return false

func _fixed_process(delta):
	if state == AGGRESSIVE:
		if current_target.get_ref():
			current_position = self.get_pos()
			enemy_position = current_target.get_ref().get_pos()
			if is_colliding_with(current_target.get_ref()):
				reached_target = true
			else:
				reached_target = false
		else:
			_on_target_dead()
	
	elif state == IDLE:
		reached_target = true
	
	if reached_target == false:
		direction = enemy_position - current_position
		self.set_pos(current_position + direction.normalized() * 100 * delta)
		self.set_rot(direction.normalized().angle()+90)

func decrease_health(amount):
	health -= amount
	health_bar.set_value(health)
	if health <= 0:
		die()

func die():
	senpai.increase_money(self)
	senpai.delete_unit(self)
	emit_signal("dead")
	queue_free()

func get_nearest_enemy():
	return senpai.get_nearest_enemy(self)

func get_team():
	return team

func set_team(t):
	team = t